module altercode/mediator

go 1.18

require (
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/fatih/color v1.10.0
	github.com/manifoldco/promptui v0.9.0
	github.com/mitchellh/go-homedir v1.0.0
	github.com/spf13/cobra v1.5.0
	github.com/spf13/viper v1.7.1
	github.com/sqweek/dialog v0.0.0-20220809060634-e981b270ebbf
	golang.org/x/sys v0.5.0
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/TheTitanrain/w32 v0.0.0-20180517000239-4f5cfb03fabf // indirect
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	github.com/pelletier/go-toml v1.2.0 // indirect
	github.com/spf13/afero v1.1.2 // indirect
	github.com/spf13/cast v1.3.0 // indirect
	github.com/spf13/jwalterweatherman v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stretchr/testify v1.8.0 // indirect
	github.com/subosito/gotenv v1.2.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/ini.v1 v1.51.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
