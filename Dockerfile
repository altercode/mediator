FROM scratch
COPY mediator /usr/bin/mediator
ENTRYPOINT ["/usr/bin/mediator"]
