# Altercode's XPOL CLI Command Line Interface


## Introduction
This Command Line Interface is made with Go Lang, using a library called [Cobra](https://github.com/spf13/cobra)

## Requirements
1. Windows

## How to get this CLI

_unavailable_
 

## Bash Autocompletion (optional)
See `xpol completion -h`

## Setup Guide (Development mode)

This guide will help you to have a fully working development environment for this cli

1. run `go get -d -v` on project folder
1. install cobra cli `go get -u github.com/spf13/cobra` so you will be able to add commands

## Release new versions

_unavailable_

## Build from source and hard installation

1. Clone this repo
1. Build from source running `go build -o ./xpol.bat main.go`
1. Make the binary executable and move it to your bin directory `sudo chmod +x ./rb && sudo mv ./rb /usr/local/bin/rb`

## Uninstall guide

_unavailable_
