package cmd

import (
	"altercode/mediator/utils"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/chzyer/readline"
	"github.com/fatih/color"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/sqweek/dialog"
	"golang.org/x/sys/windows"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"text/template"
	"time"
)

var cfgFile string

// Verbose
var Verbose bool
var version = "1.0.9"

type ConfigFile struct {
	SourceDir              string            `json:"source_dir"`
	TargetDir              string            `json:"target_dir"`
	MonthsToDirMapping     map[string]string `json:"months_mapping"`
	CategoriesToDirMapping map[string]string `json:"categories_mapping"`
	YearPrefix             string            `json:"year_prefix"`
	AdditionalPath         []string          `json:"additional_path"`
}
type CompaniesFile []struct {
	CodiceAzienda            string `json:"Codice azienda"`
	RagioneSocialeDellaDitta string `json:"Ragione sociale della ditta"`
}

type stderr struct{}

func (s *stderr) Write(b []byte) (int, error) {
	if len(b) == 1 && b[0] == 7 {
		return 0, nil
	}
	return os.Stderr.Write(b)
}

func (s *stderr) Close() error {
	return os.Stderr.Close()
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Version: version,
	Use:     "mediator",
	Short:   "Mediator CLI by Altercode",
	Long:    `This is a GO Command Line Interface made by Altercode`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		runPath, _ := filepath.Abs(".")
		//attempt to read config file
		configFilePath := filepath.Join(runPath, "mediator.json")
		companiesFilePath := filepath.Join(runPath, "companies.json")
		jsonFile, err := os.Open(configFilePath)
		defer func(jsonFile *os.File) {
			err := jsonFile.Close()
			if err != nil {
				utils.Err(err)
			}
		}(jsonFile)
		//if file is not available it will be created
		if err != nil {
			qYesNo := dialog.Message("%s", "Configurazione non trovata, vuoi crearla ora?").Title("Configurazione mancante").YesNo()
			if qYesNo {
				fromDir, errFrom := dialog.Directory().Title("Cartella sorgente").Browse()
				toDir, errTo := dialog.Directory().Title("Cartella di destinazione").Browse()
				if errFrom != nil {
					dialog.Message("%s", "Nessuna cartella iniziale selezionata").Title("Configurazione mancante").Info()
					os.Exit(1)
				}
				if errTo != nil {
					dialog.Message("%s", "Nessuna cartella finale selezionata").Title("Configurazione mancante").Info()
					os.Exit(1)
				}
				data := ConfigFile{
					SourceDir:  fromDir,
					TargetDir:  toDir,
					YearPrefix: "_ELABORAZIONE PAGHE ",
					MonthsToDirMapping: map[string]string{
						"gennaio":   "01_Gennaio",
						"febbraio":  "02_Febbraio",
						"marzo":     "03_Marzo",
						"aprile":    "04_Aprile",
						"maggio":    "05_Maggio",
						"giugno":    "06_Giugno",
						"luglio":    "07_Luglio",
						"agosto":    "08_Agosto",
						"settembre": "09_Settembre",
						"ottobre":   "10_Ottobre",
						"novembre":  "11_Novembre",
						"dicembre":  "12_Dicembre",
					},
					CategoriesToDirMapping: map[string]string{
						"cu": "CertificazioniUniche",
					},
					AdditionalPath: []string{},
				}
				configFile, _ := json.MarshalIndent(data, "", " ")
				_ = ioutil.WriteFile(configFilePath, configFile, 0644)

				jsonFile, _ = os.Open(configFilePath)
			} else {
				utils.Info("Esecuzione interrotta")
				os.Exit(0)
			}
		}

		//actually reading the config file and unmarshaling
		byteValue, _ := ioutil.ReadAll(jsonFile)
		var configFile ConfigFile
		errMarshal := json.Unmarshal(byteValue, &configFile)
		if errMarshal != nil {
			utils.Err(errMarshal)
			//time.Sleep(3 * time.Second)
			return
		}

		var companiesFile CompaniesFile
		if _, err := os.Stat(companiesFilePath); errors.Is(err, os.ErrNotExist) {
			utils.Info("companies.json non esiste, alcune aziende potrebbero non essere note")
		} else {
			jsonCompaniesFile, _ := os.Open(companiesFilePath)
			byteValueCompanies, _ := ioutil.ReadAll(jsonCompaniesFile)
			errMarshalCompanies := json.Unmarshal(byteValueCompanies, &companiesFile)
			if errMarshalCompanies != nil {
				utils.Err(errMarshalCompanies)
				//time.Sleep(3 * time.Second)
				return
			}
		}

		companies, errList := listDirectories(configFile.TargetDir)
		companyFiles := map[string][]string{}
		for _, c := range companies {
			companyFiles[c] = []string{}
		}
		sourceDir, errs := os.Open(configFile.SourceDir)
		if errs != nil {
			os.Exit(1)
		}
		defer func(sourceDir *os.File) {
			err := sourceDir.Close()
			if err != nil {
				utils.Err(err)
			}
		}(sourceDir)
		names, _ := os.ReadDir(configFile.SourceDir)
		var files []string
		var movableFiles []string
		for _, name := range names {
			if !name.IsDir() {
				filePath := fmt.Sprintf(filepath.Join(configFile.SourceDir, name.Name()))
				detectedCompany := detectCompanyFromFilename(name.Name(), companies, companiesFile)
				if detectedCompany != "" {
					companyFiles[detectedCompany] = append(companyFiles[detectedCompany], filePath)
					movableFiles = append(movableFiles, filePath)
				}
				files = append(files, filePath)
			}
		}
		if len(files) == 0 {
			utils.Warn("Nessun file trovato in " + configFile.SourceDir)
			return
		} else {
			utils.HeadLine("File totali presenti: " + strconv.Itoa(len(files)))
			utils.Succ("File pronti per essere spostati: " + strconv.Itoa(len(movableFiles)) + "/" + strconv.Itoa(len(files)))
		}
		var years []string
		t := time.Now()
		years = append(years, strconv.Itoa(t.AddDate(-1, 0, 0).Year()))
		years = append(years, strconv.Itoa(t.Year()))
		years = append(years, strconv.Itoa(t.AddDate(1, 0, 0).Year()))
		years = append(years, strconv.Itoa(t.AddDate(2, 0, 0).Year()))

		mode := promptui.Select{
			HideHelp: true,
			Label:    "Seleziona metodo",
			Items:    []string{"data", "categoria"},
		}
		_, selectedMode, errMode := mode.Run()
		if errMode != nil {
			log.Fatalf("Prompt failed %v\n", errMode)
		}
		var selectedCategory string
		var selectedYear string
		var selectedMonth string
		if selectedMode == "categoria" {
			category := promptui.Select{
				HideHelp: true,
				Label:    "Seleziona categoria",
				Items:    reflect.ValueOf(configFile.CategoriesToDirMapping).MapKeys(),
			}
			_, selectedCategory, _ = category.Run()
		} else {
			year := promptui.Select{
				HideHelp:  true,
				Label:     "Seleziona un anno e premi invio",
				Items:     years,
				CursorPos: 1,
			}
			_, selectedYear, _ = year.Run()

			month := promptui.Select{
				HideHelp:  true,
				Label:     "Seleziona un mese e premi invio",
				Items:     []string{"gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"},
				CursorPos: int(time.Now().Month()) - 1,
				Size:      12,
			}

			_, selectedMonth, _ = month.Run()
		}

		if errList != nil {
			utils.Err(errList)
			time.Sleep(3 * time.Second)
		} else {
			yearFolder := configFile.YearPrefix + selectedYear
			monthFolder := configFile.MonthsToDirMapping[selectedMonth]
			var categoryFolder string
			if selectedMode == "categoria" {
				categoryFolder = configFile.CategoriesToDirMapping[selectedCategory]
			}
			for company, companyFileList := range companyFiles {
				utils.Info("Azienda: " + company + " (" + strconv.Itoa(len(companyFileList)) + " file)")
				for _, fileToCopy := range companyFileList {
					destFile := filepath.Join(configFile.TargetDir, company, yearFolder, monthFolder, filepath.Base(fileToCopy))
					if len(configFile.AdditionalPath) > 0 {
						destFile = filepath.Join(configFile.TargetDir, company, filepath.Join(configFile.AdditionalPath...), yearFolder, monthFolder, filepath.Base(fileToCopy))
					}
					if selectedMode == "categoria" {
						destFile = filepath.Join(configFile.TargetDir, company, categoryFolder, filepath.Base(fileToCopy))
					}
					if _, err := os.Stat(destFile); err == nil {
						utils.Red(fileToCopy + " sarà sovrascritto in " + destFile)
					} else {
						utils.Warn(fileToCopy + " sarà copiato in " + destFile)
					}
				}
			}
			proceed := YesNo(YesNoParameters{YesNoLabel: "Vuoi procedere?"})
			if proceed {
				for company, companyFileList := range companyFiles {
					func() {
						for _, fileToCopy := range companyFileList {
							func() {
								destFolder := filepath.Join(configFile.TargetDir, company, yearFolder, monthFolder)
								if len(configFile.AdditionalPath) > 0 {
									destFolder = filepath.Join(configFile.TargetDir, company, filepath.Join(configFile.AdditionalPath...), yearFolder, monthFolder)
								}
								if selectedMode == "categoria" {
									destFolder = filepath.Join(configFile.TargetDir, company, categoryFolder)
								}
								destFile := filepath.Join(destFolder, filepath.Base(fileToCopy))
								_ = os.MkdirAll(destFolder, os.ModePerm)
								processedPath := filepath.Join(configFile.SourceDir, "processati")
								destFileProcessed := filepath.Join(processedPath, filepath.Base(fileToCopy))
								_ = os.MkdirAll(processedPath, os.ModePerm)
								source, errOpen := os.Open(fileToCopy)
								if errOpen != nil {
									utils.Err(errOpen)
								}
								willOverwrite := false
								if _, errStat := os.Stat(destFile); errStat == nil {
									willOverwrite = true
								} else {
									willOverwrite = false
								}
								defer func(source *os.File) {
									err := source.Close()
									if err != nil {
										utils.Err(err)
									} else {
										errRename := os.Rename(fileToCopy, destFileProcessed)
										if errRename != nil {
											utils.Err(errRename)
											return
										}
									}
								}(source)

								destination, errCreate := os.Create(destFile)
								if errCreate != nil {
									utils.Err(errCreate)
								}
								defer func(destination *os.File) {
									err := destination.Close()
									if err != nil {
										utils.Err(err)
									}
								}(destination)

								_, err := io.Copy(destination, source)

								if err != nil {
									utils.Err(err)
									return
								} else {
									if willOverwrite {
										utils.Warn(filepath.Base(fileToCopy) + " sovrascritto!")
									} else {
										utils.Succ(filepath.Base(fileToCopy) + " copiato!")
									}
								}

							}()
						}
					}()
				}
				utils.Succ("La finestra si chiuderà automaticamente tra 30 secondi...")
				time.Sleep(30 * time.Second)
			}

		}
	},
}

func detectCompanyFromFilename(filename string, companies []string, companiesFile CompaniesFile) string {
	for _, company := range companies {
		if strings.Contains(filename, company) {
			return company
		}

	}
	sort.Slice(companiesFile[:], func(i, j int) bool {
		return companiesFile[i].RagioneSocialeDellaDitta > companiesFile[j].RagioneSocialeDellaDitta
	})
	for _, company := range companiesFile {
		if strings.HasPrefix(filename, company.RagioneSocialeDellaDitta) {
			return company.CodiceAzienda
		}

	}
	return ""
}
func listDirectories(path string) ([]string, error) {
	matches, err := filepath.Glob(filepath.Join(path, "*"))
	var dirs []string
	for _, match := range matches {
		f, _ := os.Stat(match)
		if f.IsDir() {
			dirs = append(dirs, f.Name())
		}
	}
	return dirs, err
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	readline.Stdout = &stderr{}
	if err := rootCmd.Execute(); err != nil {
		utils.Err(err)
		os.Exit(1)
	}
}

func init() {
	stdout := windows.Handle(os.Stdout.Fd())
	var originalMode uint32

	_ = windows.GetConsoleMode(stdout, &originalMode)
	_ = windows.SetConsoleMode(stdout, originalMode|windows.ENABLE_VIRTUAL_TERMINAL_PROCESSING)

	cobra.OnInitialize(initConfig)
	cobra.AddTemplateFuncs(template.FuncMap{
		"FG_Bold":      color.New(color.Bold).SprintFunc(),
		"FG_Underline": color.New(color.Underline).SprintFunc(),
		"FG_Green":     color.New(color.FgGreen).SprintFunc(),
		"FG_Yellow":    color.New(color.FgYellow).SprintFunc(),
		"FG_Cyan":      color.New(color.FgCyan).SprintFunc(),
		"FG_Red":       color.New(color.FgRed).SprintFunc(),
	})

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "verbose output")

	rootCmd.SetHelpTemplate(`{{with (or .Long .Short)}}{{. | trimTrailingWhitespaces  | FG_Bold }}

{{end}}{{if or .Runnable .HasSubCommands}}{{.UsageString}}{{end}}`)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	viper.AutomaticEnv() // read in environment variables that match

}

type YesNoParameters struct {
	YesNoLabel string `default:"Select[Sì/No]"`
}

func YesNo(prm YesNoParameters) bool {
	prompt := promptui.Select{
		HideHelp: true,
		Label:    prm.YesNoLabel,
		Items:    []string{"Sì", "No"},
	}
	_, result, err := prompt.Run()
	if err != nil {
		log.Fatalf("Prompt failed %v\n", err)
	}
	return result == "Sì"
}
